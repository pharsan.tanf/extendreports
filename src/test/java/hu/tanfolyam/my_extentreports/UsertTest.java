package hu.tanfolyam.my_extentreports;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import hu.tanfolyam.my_extentreports.User;

class UsertTest {
	
	private static User user;
	
	@BeforeAll
	static void setupAll() {
		user = new User("John", "Doe");
	}

	@Test
	void testUser() {
		User actUser = new User("John", "Doe");
		assertThat(actUser.getId()).isEqualTo(2);
		assertThat(actUser.getFirstName()).isEqualTo("John");
		assertThat(actUser.getLastName()).isEqualTo("Doe");
	}

	@Test
	void testGetId() {
		assertThat(user.getId()).isEqualTo(1);
	}

	@Test
	void testGetFirstName() {
		assertThat(user.getFirstName()).isEqualTo("John");
	}

	@Test
	void testGetLastName() {
		assertThat(user.getLastName()).isEqualTo("Doe");
	}

}
