package hu.tanfolyam.my_extentreports;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

import hu.tanfolyam.my_extentreports.User;
import hu.tanfolyam.my_extentreports.UserService;

public class UserServiceTest {
	
	static ExtentReports extent;
	
	@BeforeAll
	static void setupAll() {
		ExtentSparkReporter spark = new ExtentSparkReporter("target/Spark.html");
		extent = new ExtentReports();
		extent.attachReporter(spark);
	}

    @Test
    void testAddUser() {
    	ExtentTest test = extent.createTest("Add user testcase", "Testing UserService class add user.");
        // Arrange
    	test.info("Start add user testcase.");
        UserService userService = new UserService();
        User user = new User("John", "Doe");
        test.info("John Doe created.");

        // Act
        userService.addUser(user);
        test.info("John Doe added.");

        // Assert
        assertThat(userService.getUserCount()).isEqualTo(1);
        test.pass("UserCount is equals to 1");
        assertThat(userService.getUserById(user.getId())).isEqualTo(user);
        test.pass("UserID is equals to " + user.getId());
    }

    @Test
    void testRemoveUser() {
    	ExtentTest test = extent.createTest("Remove user testcase", "Testing UserService class add user.");
        // Arrange
    	test.info("Start remove user testcase.");
        UserService userService = new UserService();
        User user = new User("John", "Doe");
        userService.addUser(user);
        test.info("John Doe added.");

        // Act
        userService.removeUser(user.getId());
        test.info("John Doe removed.");

        // Assert
        assertThat(userService.getUserCount()).isEqualTo(0);
        test.pass("UserCount is equals to 0");
        assertThat(userService.getUserById(user.getId())).isNull();
        test.pass("UserID is equals to " + null);
    }

    @Test
    void testGetAllUsers() {
    	ExtentTest test = extent.createTest("Get all users testcase", "Testing UserService class get all users.");
        // Arrange
    	test.info("Start get all users testcase.");
        UserService userService = new UserService();
        User user1 = new User("John", "Doe");
        User user2 = new User("Jane", "Smith");
        test.info("Two users created.");
        userService.addUser(user1);
        userService.addUser(user2);
        test.info("Two users added.");

        // Act
        List<User> users = userService.getAllUsers();
        test.info("GetAllUsers executed.");

        // Assert
        assertThat(users).hasSize(2); 
        test.pass("UserCount is equals to 2");
        assertThat(users).containsExactlyInAnyOrder(user1, user2);
        test.pass("The list of users contains exactly the two expected elements.");
//        assertThat(users).containsExactlyInAnyOrder(user1);
//        assertThat(users).containsExactly(user2, user1);
    }
    
    @AfterAll
    static void tearDownAll() {
    	extent.flush();
    }
}
