package hu.tanfolyam.my_extentreports;

import java.util.ArrayList;
import java.util.List;

class UserService {
	
    private List<User> users;

    public UserService() {
        users = new ArrayList<>();
    }

    public void addUser(User user) {
        users.add(user);
    }

    public void removeUser(int userId) {
        users.removeIf(user -> user.getId() == userId);
    }

    public int getUserCount() {
        return users.size();
    }

    public User getUserById(int userId) {
        for (User user : users) {
            if (user.getId() == userId) {
                return user;
            }
        }
        return null;
    }

    public List<User> getAllUsers() {
        return new ArrayList<>(users);
    }
}

