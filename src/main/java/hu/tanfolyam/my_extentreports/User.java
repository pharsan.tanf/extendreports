package hu.tanfolyam.my_extentreports;

class User {
	
    private static int counter = 0;
    private int id;
    private String firstName;
    private String lastName;

    public User(String firstName, String lastName) {
        this.id = ++counter;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}